<?php
/**
* @project    Atom-M CMS
* @package    News Entity
* @url        https://atom-m.net
*/


namespace FotoModule\ORM;

class FotoEntity extends \OrmEntity
{
    protected $id;
    protected $title;
    protected $description;
    protected $views;
    protected $date;
    protected $category_id;
    protected $category = null;
    protected $author_id;
    protected $author = null;
    protected $comments;
    protected $commented;
    protected $filename = null;



    public function save()
    {
        $params = array_merge(array(
            'title' => $this->title,
            'description' => $this->description,
            'views' => intval($this->views),
            'date' => $this->date,
            'category_id' => $this->category_id,
            'author_id' => intval($this->author_id),
            'comments' => (!empty($this->comments)) ? intval($this->comments) : 0,
            'commented' => (!empty($this->commented)) ? 1 : 0,
            'filename' => $this->filename,
        ), 
            \AtmAddFields::selectFromArray($this->asArray())
        );

        if ($this->id) $params['id'] = $this->id;

        return (getDB()->save('foto', $params));
    }



    public function delete()
    {
        $commentsModel = \OrmManager::getModelInstance('Comments');
        $commentsModel->deleteByParentId($this->id, 'foto');

        $path_files = ROOT . '/data/files/foto/' . $this->filename;
        $path_images = ROOT . '/data/images/foto/' . $this->filename;
        if (file_exists($path_files)) {
            unlink($path_files);
        } elseif (file_exists($path_images)) {
            unlink($path_images);
        }

        if (\Config::read('use_local_preview', 'foto')) {
            $preview = \Config::read('use_preview', 'foto');
            $size_x = \Config::read('img_size_x', 'foto');
            $size_y = \Config::read('img_size_y', 'foto');
        } else {
            $preview = \Config::read('use_preview');
            $size_x = \Config::read('img_size_x');
            $size_y = \Config::read('img_size_y');
        }
        $path = ROOT.'/data/images/foto/'.$size_x.'x'.$size_y.'/'.$this->filename;
        if (file_exists($path)) unlink($path);

        getDB()->delete('foto', array('id' => $this->id));
    }


    /**
     * @param $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }



    /**
     * @return object
     */
    public function getAuthor()
    {
        if (!$this->checkProperty('author')) {
            if (!$this->getAuthor_id()) {
                $this->author = \OrmManager::getEntityInstance('users');
            } else {
                $this->author = \OrmManager::getModelInstance('Users')->getById($this->author_id);
            }
        }
        return $this->author;
    }



    /**
     * @param $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }



    public function __getAPI() {

        if (
            !$this->available ||
            !\ACL::turnUser(array('foto', 'view_list')) ||
            !\ACL::turnUser(array('foto', 'view_materials'))
        )
            return array();

        $categories = $this->getCategories();
        foreach($categories as $category)
            if (\ACL::checkAccessInList($category->getNo_access()))
                return array();

        return array_merge(array(
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'views' => $this->views,
            'date' => $this->date,
            'category_id' => $this->category_id,
            'author_id' => $this->author_id,
            'comments' => $this->comments,
            'commented' => $this->commented,
            'filename' => $this->filename,
        ), 
            \AtmAddFields::selectFromArray($this->asArray())
        );
    }
}