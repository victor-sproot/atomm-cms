<?php
return array(
    'icon_class' => 'mdi-communication-chat',
    'pages'   => array(
        WWW_ROOT.'/admin/settings.php?m=chat' => __('Settings'),
        WWW_ROOT.'/admin/design.php?m=chat'   => __('Design'),
    ),
);
