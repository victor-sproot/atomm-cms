<!DOCTYPE html>
<?php
    if (!empty($_SESSION['user']['name'])) {
        $ava_path = getAvatar($_SESSION['user']['id'], $_SESSION['user']['email']);
    }
    @ini_set('default_socket_timeout', 5);
?>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        
        <title><?php echo $pageTitle ?></title>
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
        
        <!--Import materialize.css-->
        <link type="text/css" rel="stylesheet" href="<?php echo WWW_ROOT ?>/admin/template/css/materialize.min.css"  media="screen,projection"/>
        <link type="text/css" rel="stylesheet" href="<?php echo WWW_ROOT ?>/admin/template/css/main.css"  media="screen,projection"/>

        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
        
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    </head>

    <body>
        <header>
        <div class="navbar-fixed">
        <nav>
            <div class="nav-wrapper light-blue darken-4">
                <a href="<?php echo WWW_ROOT ?>/admin" class="brand-logo"><img src="<?php echo WWW_ROOT ?>/admin/template/img/logo.png" alt="Logo Atom-M"/></a>
                <span style="padding-left: 4rem;">
                    Concept Admin
                </span>
                <ul class="right hide-on-med-and-down">
                    <li><a class="dropdown-button waves-effect" href="" data-activates="main_navmenu" data-constrainwidth="false" data-beloworigin="true"><?php echo __('General'); ?></a></li>
                    <ul id="main_navmenu" class="dropdown-content">
                        <li><a class="waves-effect" href="<?php echo WWW_ROOT ?>/admin/settings.php?m=__sys__"><?php echo __('Common settings'); ?></a></li>
                        <li><a class="waves-effect" href="<?php echo WWW_ROOT ?>/admin/clean_cache.php"><?php echo __('Clean cache'); ?></a></li>
                    </ul>
                    <li><a class="dropdown-button waves-effect" href="" data-activates="plugins_navmenu" data-constrainwidth="false" data-beloworigin="true"><?php echo __('Plugins'); ?></a></li>
                    <ul id="plugins_navmenu" class="dropdown-content">
                        <li><a class="waves-effect" href="<?php echo WWW_ROOT ?>/admin/plugins.php" style="text-align: center"><?php echo __('Plugins manager'); ?></a></li>
                        <li class="divider"></li>
                        <?php
                            $Cache = new Cache;
                            $Cache->lifeTime = 6000;
                            if ($Cache->check('adm_pl_settings')) {
                                $list = $Cache->read('adm_pl_settings');
                            } else {
                                $plugins = glob(ROOT . '/plugins/*');
                                $list = '';
                                if (!empty($plugins) && count($plugins) > 0) {
                                    foreach ($plugins as $pl) {
                                        if (file_exists($pl . '/config.json')) {
                                            $config = json_decode(file_get_contents($pl . '/config.json'), 1);
                                            if (!empty($config['active'])) {
                                                $settigs_file_path = $pl . '/settings.php';
                                                if (file_exists($settigs_file_path))
                                                    $list .= "<li><a class=\"waves-effect\" href=\"/admin/plugins.php?ac=edit&dir=".substr($pl, strripos($pl, '/')+1)."\">".h($config['title'])."</a></li>";
                                            }
                                        }
                                    }
                                }
                                $Cache->write($list, 'adm_pl_settings', array());
                            }
                            echo $list;
                        ?>
                    </ul>
                    <li><a class="dropdown-button waves-effect" href="" data-activates="snippets_navmenu" data-constrainwidth="false" data-beloworigin="true"><?php echo __('Snippets'); ?></a></li>
                    <ul id="snippets_navmenu" class="dropdown-content">
                        <li><a class="waves-effect" href="<?php echo WWW_ROOT ?>/admin/snippets.php"><?php echo __('Create'); ?></a></li>
                        <li><a class="waves-effect" href="<?php echo WWW_ROOT ?>/admin/snippets.php?a=ed"><?php echo __('Edit'); ?></a></li>
                    </ul>
                    <li><a class="dropdown-button waves-effect" href="" data-activates="design_navmenu" data-constrainwidth="false" data-beloworigin="true"><?php echo __('Design'); ?></a></li>
                    <ul id="design_navmenu" class="dropdown-content">
                        <li><a class="waves-effect" href="<?php echo WWW_ROOT ?>/admin/design.php?d=default&t=main"><?php echo __('General design and css'); ?></a></li>
                        <li><a class="waves-effect" href="<?php echo WWW_ROOT ?>/admin/menu_editor.php"><?php echo __('Menu editor'); ?></a></li>
                        <li><a class="waves-effect" href="<?php echo WWW_ROOT ?>/admin/errors_tmp.php"><?php echo __('Error pages'); ?></a></li>
                    </ul>
                    <li><a class="dropdown-button waves-effect" href="" data-activates="security_navmenu" data-constrainwidth="false" data-beloworigin="true"><?php echo __('Security'); ?></a></li>
                    <ul id="security_navmenu" class="dropdown-content">
                        <li><a class="waves-effect" href="<?php echo WWW_ROOT ?>/admin/settings.php?m=__secure__"><?php echo __('Security settings'); ?></a></li>
                        <li><a class="waves-effect" href="<?php echo WWW_ROOT ?>/admin/system_log.php"><?php echo __('Action log'); ?></a></li>
                        <li><a class="waves-effect" href="<?php echo WWW_ROOT ?>/admin/ip_ban.php"><?php echo __('Bans by IP'); ?></a></li>
                        <li><a class="waves-effect" href="<?php echo WWW_ROOT ?>/admin/dump.php"><?php echo __('Backup control'); ?></a></li>
                        <li><a class="waves-effect" href="<?php echo WWW_ROOT ?>/admin/rules.php"><?php echo __('Rules editor'); ?></a></li>
                    </ul>
                    <li><a class="dropdown-button waves-effect" href="" data-activates="other_navmenu" data-constrainwidth="false" data-beloworigin="true"><?php echo __('Additional'); ?></a></li>
                    <ul id="other_navmenu" class="dropdown-content">
                        <li><a class="waves-effect" href="<?php echo WWW_ROOT ?>/admin/settings.php?m=__seo__"><?php echo __('SEO settings'); ?></a></li>
                        <li><a class="waves-effect" href="<?php echo WWW_ROOT ?>/admin/settings.php?m=__rss__"><?php echo __('RSS settings'); ?></a></li>
                        <li><a class="waves-effect" href="<?php echo WWW_ROOT ?>/admin/settings.php?m=__sitemap__"><?php echo __('Sitemap settings'); ?></a></li>
                        <li><a class="waves-effect" href="<?php echo WWW_ROOT ?>/admin/settings.php?m=__preview__"><?php echo __('Preview settings'); ?></a></li>
                        <li><a class="waves-effect" href="<?php echo WWW_ROOT ?>/admin/settings.php?m=__watermark__"><?php echo __('Watermark settings'); ?></a></li>
                        <li><a class="waves-effect" href="<?php echo WWW_ROOT ?>/admin/additional_fields.php?m=comments"><?php echo __('Add fields comments'); ?></a></li>
                    </ul>
                    <li><a class="dropdown-button waves-effect" href="" data-activates="helps_navmenu" data-constrainwidth="false" data-beloworigin="true"><?php echo __('Help'); ?></a></li>
                    <ul id="helps_navmenu" class="dropdown-content">
                        <li><a class="waves-effect" href="https://dev.atom-m.net" target="_blank"><?php echo __('Atom-M CMS Community'); ?></a></li>
                        <li><a class="waves-effect" href="https://bitbucket.org/atom-m/cms/wiki/Home" target="_blank"><?php echo __('Wiki'); ?></a></li>
                        <li><a class="waves-effect" href="<?php echo WWW_ROOT ?>/admin/faq.php"><?php echo __('FAQ'); ?></a></li>
                        <li><a class="waves-effect" href="<?php echo WWW_ROOT ?>/admin/authors.php"><?php echo __('Dev. Team'); ?></a></li>
                    </ul>
                    <li><a class="dropdown-button waves-effect" href="" data-activates="profile_navmenu" data-constrainwidth="false" data-beloworigin="true"><i class="col s2 right"><img src="<?php echo $ava_path; ?>" alt="Avatar" class="circle responsive-img"/></i><?php echo h($_SESSION['user']['name']); ?></a></li>
                    <ul id="profile_navmenu" class="dropdown-content">
                        <li><a class="waves-effect" href="<?php echo WWW_ROOT ?>/"><?php echo __('Return to site') ?></a></li>
                        <li><a class="waves-effect" href="<?php echo WWW_ROOT ?>/admin/exit.php"><?php echo __('Sign out') ?></a></li>
                    </ul>
                </ul>
                <!-- For mobile -->
                <!--
                <a href="#" data-activates="mobile-navmenu" class="button-collapse"><i class="mdi-navigation-menu"></i></a>
                <ul class="side-nav collapsible" data-collapsible="accordion" id="mobile-navmenu">
                    <li class="bold no-padding"><a class="collapsible-header"><i class="mdi-action-view-module"></i>MrBoriska</a>
                      <div class="collapsible-body transparent">
                        <ul class="no-padding">
                        <li class="no-padding"><a href="#!">На сайт</a></li>
                        <li class="no-padding"><a href="#!">Выйти</a></li>
                        </ul>
                      </div>
                    </li>
                    <li class="bold no-padding"><a class="collapsible-header">Общие</a>
                      <div class="collapsible-body transparent">
                        <ul>
                        <li class="no-padding"><a href="#!">Общие настройки</a></li>
                        <li class="no-padding"><a href="#!">Очистить кеш</a></li>
                        </ul>
                      </div>
                    </li>
                    <li class="bold no-padding"><a class="collapsible-header">Плагины</a>
                      <div class="collapsible-body transparent">
                        <ul class="no-padding">
                        <li class="no-padding"><a href="#!">Менеджер плагинов</a></li>
                        <li class="divider"></li>
                        <li class="no-padding"><a href="#!">BBCodes Editor</a></li>
                        <li class="no-padding"><a href="#!">Генератор тегов</a></li>
                        <li class="no-padding"><a href="#!">Опросы v.2</a></li>
                        <li class="no-padding"><a href="#!">uLogin</a></li>
                        <li class="no-padding"><a href="#!">Топ пользователей</a></li>
                        </ul>
                      </div>
                    </li>
                    <li class="bold no-padding"><a class="collapsible-header">Сниппеты</a>
                      <div class="collapsible-body transparent">
                        <ul class="no-padding">
                        <li class="no-padding"><a href="#!">Создать</a></li>
                        <li class="no-padding"><a href="#!">Редактировать</a></li>
                        </ul>
                      </div>
                    </li>
                    <li class="bold no-padding"><a class="collapsible-header">Дизайн</a>
                      <div class="collapsible-body transparent">
                        <ul class="no-padding">
                        <li class="no-padding"><a href="#!">Общий дизайн</a></li>
                        <li class="no-padding"><a href="#!">Редактор меню</a></li>
                        <li class="no-padding"><a href="#!">Страницы ошибок</a></li>
                        </ul>
                      </div>
                    </li>
                    <li class="bold no-padding"><a class="collapsible-header">Безопасность</a>
                      <div class="collapsible-body transparent">
                        <ul class="no-padding">
                        <li class="no-padding"><a href="#!">Параметры</a></li>
                        <li class="no-padding"><a href="#!">Лог действий</a></li>
                        <li class="no-padding"><a href="#!">Блокировка по IP</a></li>
                        <li class="no-padding"><a href="#!">Backups</a></li>
                        <li class="no-padding"><a href="#!">Редактор прав</a></li>
                        </ul>
                      </div>
                    </li>
                    <li class="bold no-padding"><a class="collapsible-header">Дополнительно</a>
                      <div class="collapsible-body transparent">
                        <ul class="no-padding">
                        <li class="no-padding"><a href="#!">Управление SEO</a></li>
                        <li class="no-padding"><a href="#!">RSS</a></li>
                        <li class="no-padding"><a href="#!">Sitemap</a></li>
                        <li class="no-padding"><a href="#!">Генератор миниатюр</a></li>
                        <li class="no-padding"><a href="#!">Водяные знаки</a></li>
                        <li class="no-padding"><a href="#!">Доп. поля комментариев</a></li>
                        </ul>
                      </div>
                    </li>
                    <li class="bold no-padding"><a class="collapsible-header">Помощь</a>
                      <div class="collapsible-body transparent">
                        <ul class="no-padding">
                        <li class="no-padding"><a href="#!">Сообщество AtomX</a></li>
                        <li class="no-padding"><a href="#!">Сообщество AtomM</a></li>
                        <li class="no-padding"><a href="#!">Документация</a></li>
                        <li class="no-padding"><a href="#!">FAQ</a></li>
                        <li class="no-padding"><a href="#!">Авторы</a></li>
                        </ul>
                      </div>
                    </li>
                </ul>
                -->
            </div>
        </nav>
        </div>
        </header>

        <!-- Page Layout here -->
        <main>
        <div class="row no-margin blue-grey darken-4 flex">
            <div class="col s12 m4 l3 grey-text no-padding"> <!-- Note that "m4 l3" was added -->
            <!-- Grey navigation panel

              This content will be:
            3-columns-wide on large screens,
            4-columns-wide on medium screens,
            12-columns-wide on small screens  -->
                <ul class="main-menu collapsible z-depth-0 b15tm" data-collapsible="expandable">
                    <?php
                    
                    $modsInstal = new ModuleInstaller;
                    
                    // Неустановленные модули
                    $newmodules = $modsInstal->checkNewModules();

                    if (count($newmodules)):
                        foreach ($newmodules as $n => $module):
                    ?>
                        <li>
                            <div class="collapsible-header">
                                <i class="mdi-action-get-app red-text text-accent-1"></i>
                                <?php echo __($module,false,$module); ?>
                            </div>
                            <div class="collapsible-body">
                                <div class="collection no-margin">
                                    <a class="collection-item" href="<?php echo WWW_ROOT; ?>/admin?install=<?php echo $module ?>"><?php echo __('Install') ?></a>
                                </div>
                            </div>
                        </li>
                    <?php
                        endforeach;
                    endif;

                    $modules = getAdmFrontMenuParams();
                    
                    // Активные модули
                    $i=0;
                    foreach ($modules as $modKey => $modData): 
                        if (!empty($nsmods) && array_key_exists($modKey, $nsmods)) continue;
                        if (Config::read('active', $modKey) != 1) continue;
                        $i=1;
                    ?>
                        <li>
                            <div class="collapsible-header waves-effect">
                                <?php if (isset($modData['icon_url'])) {?>
                                    <i><img src="<?php echo $modData['icon_url']; ?>" alt="icon" class="responsive-img"></i>
                                <?php } else { ?>
                                    <i class="<?php echo isset($modData['icon_class']) ? $modData['icon_class'] : 'mdi-action-extension'; ?>"></i>
                                <?php } ?>
                                <?php if (count($modData['pages']) < 2) { ?>
                                    <a href="<?php echo key($modData['pages']); ?>"><?php echo $modData['title']; ?></a>
                                <?php } else { ?>
                                    <?php echo $modData['title']; ?>
                                    <i class="mdi-navigation-arrow-drop-down right"></i>
                                <?php } ?>
                            </div>
                            <?php if (count($modData['pages']) > 1) { ?>
                                <div class="collapsible-body">
                                    <div class="collection no-margin">
                                        <?php foreach ($modData['pages'] as $url => $ankor): ?>
                                            <a class="collection-item waves-effect grey-text" href="<?php echo $url; ?>"><?php echo $ankor; ?></a>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            <?php } ?>
                        </li>
                    <?php endforeach;

                    // Неактивные модули
                    $i=0;
                    foreach ($modules as $modKey => $modData):
                        if (!empty($nsmods) && array_key_exists($modKey, $nsmods)) continue;
                        if (Config::read('active', $modKey) == 1) continue;
                        $i=1;
                    ?>
                        <li class="grey-text text-darken-2">
                            <div class="collapsible-header waves-effect">
                                <i class="<?php echo isset($modData['icon_class']) ? $modData['icon_class'] : 'mdi-action-extension'; ?>"></i>
                                <?php if (count($modData['pages']) < 2) { ?>
                                    <a href="<?php echo key($modData['pages']); ?>"><?php echo $modData['title']; ?></a>
                                <?php } else { ?>
                                    <?php echo $modData['title']; ?>
                                    <i class="mdi-navigation-arrow-drop-down right"></i>
                                <?php } ?>
                            </div>
                            <?php if (count($modData['pages']) > 1) { ?>
                                <div class="collapsible-body">
                                    <div class="collection no-margin">
                                        <?php foreach ($modData['pages'] as $url => $ankor): ?>
                                            <a class="collection-item waves-effect grey-text text-darken-1" href="<?php echo $url; ?>"><?php echo $ankor; ?></a>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            <?php } ?>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>

            <div class="col s12 m8 l9 z-depth-1 flex-1-0-auto grey lighten-4 b30bp"> <!-- Note that "m8 l9" was added -->
            <?php if (isset($pageNav) || isset($pageNavr)) { ?>
                <div class="row b15tm">
                    <div class="col s6">
                        <?php echo $pageNav; ?>
                    </div>
                    <div class="col s6 right-align">
                        <?php echo $pageNavr; ?>
                    </div>
                </div>
            <?php } ?>
            <!-- Teal page content

              This content will be:
            9-columns-wide on large screens,
            8-columns-wide on medium screens,
            12-columns-wide on small screens  -->
