<?php
@ini_set('display_errors', 0);

function red($s) {
    return '<span style="color:#FF0000">'.$s.'</span>';
}

$out = '';
$errors = array();

$php = explode('.', phpversion());
if ($php[0]<5 or $php[1]<4) {
    $errors['PHP v'.phpversion()] = 'Требуется PHP версии не ниже 5.4.0';
}

if (ini_get('safe_mod') == 1) {
    $errors['safe_mod'] = 'Возможности сервера ограничены';
}

if (!function_exists('set_time_limit')) {
    $errors['set_time_limit()'] = 'Понадобится при быстром росте сайта';
}

if (!function_exists('chmod')) {
    $errors['chmod()'] = 'Необходимо для смены прав на файлы и папки';
}

if (!function_exists('imagecreatetruecolor')) {
    $errors['imagecreatetruecolor()'] = 'Необходимо для обработки изображений';
}

if (!function_exists('imageCopy')) {
    $errors['imageCopy()'] = 'Необходимо для обработки изображений';
}

if (!function_exists('imageGIF')) {
    $errors['imageGIF()'] = 'Необходимо для обработки изображений';
}

if (!function_exists('imageJPEG')) {
    $errors['imageJPEG()'] = 'Необходимо для обработки изображений';
}

if (!function_exists('imagePNG')) {
    $errors['imagePNG()'] = 'Необходимо для обработки изображений';
}

if (!function_exists('imagecreatefromjpeg')) {
    $errors['imagecreatefromjpeg()'] = 'Необходимо для обработки изображений';
}

if (!function_exists('imagecreatefromgif')) {
    $errors['imagecreatefromgif()'] = 'Необходимо для обработки изображений';
}

if (!function_exists('imagecreatefrompng')) {
    $errors['imagecreatefrompng()'] = 'Необходимо для обработки изображений';
}

if (!function_exists('imagesx')) {
    $errors['imagesx()'] = 'Необходимо для обработки изображений';
}

if (!function_exists('imagesy')) {
    $errors['imagesy()'] = 'Необходимо для обработки изображений';
}

if (!function_exists('imageDestroy')) {
    $errors['imageDestroy()'] = 'Необходимо для обработки изображений';
}

if (!function_exists('getImageSize')) {
    $errors['getImageSize()'] = 'Необходимо для обработки изображений';
}

if (!function_exists('exif_imagetype') && !function_exists('getImageSize')) {
    $errors['exif_imagetype()'] = 'Необходимо для обработки изображений';
}

if (!function_exists('imagecopyresampled')) {
    $errors['imagecopyresampled()'] = 'Необходимо для обработки изображений';
}

if (!function_exists('imagecolorsforindex')) {
    $errors['imagecolorsforindex()'] = 'Необходимо для обработки изображений';
}

if (!function_exists('imagecolorat')) {
    $errors['imagecolorat()'] = 'Необходимо для обработки изображений';
}

if (!function_exists('imagesetpixel')) {
    $errors['imagesetpixel()'] = 'Необходимо для обработки изображений';
}

if (!function_exists('imagecolorclosest')) {
    $errors['imagecolorclosest()'] = 'Необходимо для обработки изображений';
}

if (!extension_loaded( "openssl" )) {
    $errors['php_openssl'] = 'Необходимо для установления защищённого соединения в каталоге плагинов';
}


if (count($errors)>0) {
    foreach ($errors as $title => $desc) {
        echo '<span style="color:#FF0000">'.$title.'</span> - '.$desc.'<br>';
    }
} else {
    echo '<span style="color:#46B100">Ваш сервер настроен идеально! :)</span><br />';
}

?>






