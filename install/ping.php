<?php
include_once '../sys/boot.php';

@ini_set('display_errors', 0);
@ini_set('default_socket_timeout', 5);

// номер версии и адрес хоста отправляются ТОЛЬКО в целях ведения внутренней статистики количества установок движка
@$m = file_get_contents('https://atom-m.net/last.php?v=' . ATOM_VERSION . '&host=' . $_SERVER['HTTP_HOST']);
if ($m && preg_match('#[^></]+#i', $m)) {
    if ($m == ATOM_VERSION)
        echo 'Вы используете последнюю версию Atom-M ' . trim($m);
    else
        echo '<a href="https://atom-m.net">Рекомендуем обновить до Atom-M ' . trim($m) . '</a>';
} else {
    echo 'Не удалось узнать';
}

?>