<?php

$output    = '';
$template_patch = dirname(__FILE__).'/template/comments.html';
$conf_pach = dirname(__FILE__).'/config.json';
$config = json_decode(file_get_contents($conf_pach), true);

if (isset($_POST['send'])) {

    $config['limit'] = $_POST['limit'];
    $config['shot_comm'] = $_POST['shot_comm'];

    file_put_contents($conf_pach, json_encode($config));
    $f = fopen($template_patch, "w");
    fwrite($f,$_POST['template']);

    $Cache = new Cache;
    $Cache->remove('pl_last_comments');

    $output .= '<div class="warning">Изменения приняты!</div>';
}

$template = file_get_contents($template_patch);

$output .= '<style>
                .ib {
                    font-weight: bold;
                    font-style: italic;
                }
                .right {
                    width: 50%;
                }
                input[type="text"] {
                    height: auto;
                }
                .markers input {
                    background: #D9D9D9;
                    border-radius: 5px;
                    display: inline-block;
                    font-weight: 700;
                    padding: 5px;border:none;width:130px;text-align:center;
                }
                .mmand {
                    color:#262626;
                    font-weight:bold;
                    text-align:center;
                    padding-top:4px;
                }
            </style>';

$output .= '<form action="" method="post">
                <div class="list">
                    <div class="title">Управление плагином Топ пользователей</div>
                    <div class="level1">

                            <div class="items">
                                <div class="setting-item">
                                    <div class="title" style="padding: 20px; text-align:left">Метка для вывода топа пользователей {{ last_comments }}</div>
                                </div>
                            </div>

                            <div class="items">
                                <div class="setting-item">
                                    <div class="left">Сколько комментариев выводить в топе:</div>
                                    <div class="right">
                                        <input type="text" size="100" name="limit" value="' . $config['limit'] . '">
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>

                          <div class="items">
                                <div class="setting-item">
                                    <div class="left">Ограничение длины комментариев: </div>
                                    <div class="right">
                                        <input type="text" size="100" name="shot_comm" value="' . $config['shot_comm'] . '">
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>


                            <div class="items">
                                <div class="setting-item">
                                    <textarea name="template" style="width: 99%; height: 350px">' . (isset($template) ? htmlspecialchars($template) : '') . '</textarea>
                                </div>
                            </div>

                            <div class="items">
                                <div class="setting-item">
                                    <div class="title" style="padding: 20px; text-align:center; height: auto;"><input name="send" type="submit" value="Сохранить" class="save-button"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>';

$output .= '<ul class="markers">
                <li><input onclick="select(this)" value="{{ comm.avatar }}" readonly="" /> - ссылка на аватар пользователя, если нету выводит noavatar</li>
                <li><input onclick="select(this)" value="{{ comm.number }}" readonly="" /> - Выводит порядковый номер комментария в блоке</li>
                <li><input onclick="select(this)" value="{{ comm.title }}" readonly="" /> - Название материала к которому добавлен комментарий</li>
                <li><input onclick="select(this)" value="{{ comm.module }}" readonly="" /> - Выводит модуль, к которому добавлен комментарий (Например: "К фотографии")</li>
                <li><input onclick="select(this)" value="{{ comm.date }}" readonly="" /> - Дата добавления </li>
                <li><input onclick="select(this)" value="{{ comm.url }}" readonly="" /> - Адрес материала, к которому добавлен комментарий </li>
                <li><input onclick="select(this)" value="{{ comm.name }}" readonly="" /> - Автор комментария</li>
                <li><input onclick="select(this)" value="{{ comm.message}}" readonly="" /> - Текст комментария</li>

            </ul>
            <div class="mmand">Версия 0.2 <br /> Специально для Atom-M CMS</div>
            ';

?>