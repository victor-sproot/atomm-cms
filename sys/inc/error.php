<?php

$code = http_response_code();

if (is_numeric($code)) {
    $headers = array(
        '404' => "HTTP/1.0 404 Not Found",
        '403' => "HTTP/1.0 403 Forbidden You don't have permission to access / on this server.",
    );
    if (!empty($headers[$code]))  header($headers[$code]);
}
include_once $_SERVER['DOCUMENT_ROOT'].'/sys/boot.php';

switch ($code) {
    case 'hack':
        $html = @file_get_contents(ROOT . '/data/errors/hack.html');
        break;
    case '403':
        $html = @file_get_contents(ROOT . '/data/errors/403.html');
        break;
    case '404':
        $html = @file_get_contents(ROOT . '/data/errors/404.html');
        break;
    case 'ban':
        $html = @file_get_contents(ROOT . '/data/errors/ban.html');
        break;
    default:
        $html = @file_get_contents(ROOT . '/data/errors/default.html');
}

$Viewer = new Viewer_Manager();

$markers = array();
$markers['code'] = $code;
$markers['site_title'] = Config::read('site_title');
$markers['site_domain'] = $_SERVER['SERVER_NAME'];

echo $Viewer->parseTemplate($html, array('error' => $markers));

?>