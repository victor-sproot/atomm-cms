<?php
/**
* @project    Atom-M CMS
* @package    Filters
* @url        https://atom-m.net
*/


class Viewer_Filter_Shift {


    public function compile($value, Viewer_CompileParser $compiler)
    {
        if (!is_callable($value)) throw new Exception('(Filter_Shift):Value for filtering must be callable.');

        $compiler->raw('array_shift(');
        $value($compiler);
        $compiler->raw(')');
    }
    
    
    public function __toString()
    {
        $out = '[filter]:shift' . "\n";
        return $out;
    }
}