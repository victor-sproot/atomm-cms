<?php
/**
* @project    Atom-M CMS
* @package    AddFields Entity
* @url        https://atom-m.net
*/


namespace ORM;

class AddFieldsModel extends \OrmModel
{

    public $Table = 'add_fields';

    protected $RelatedEntities;
}